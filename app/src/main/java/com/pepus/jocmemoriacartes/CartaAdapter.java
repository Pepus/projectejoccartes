package com.pepus.jocmemoriacartes;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CartaAdapter extends RecyclerView.Adapter<CartaAdapter.CartaViewHolder>{
    private List<Carta> cartes;
    private Context context;
    private Carta carta1;
    private Carta carta2;
    private int torn = 0;

    public class CartaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView imatge;


        public CartaViewHolder(View itemView) {
            super(itemView);
            imatge = itemView.findViewById(R.id.imagen);
            imatge.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (torn == 0) {
                carta1 = cartes.get(this.getAdapterPosition());
                carta1.girar();
                notifyDataSetChanged();
                torn++;
            } else if (torn == 1) {
                carta2 = cartes.get(this.getAdapterPosition());
                carta2.girar();
                notifyDataSetChanged();
                torn++;

                if (carta1.getImage() == carta2.getImage()) {
                    carta1.setEstat(Carta.Estat.FIXED);
                    carta2.setEstat(Carta.Estat.FIXED);
                    torn = 0;
                    notifyDataSetChanged();
                    if (haGanado()) {
                        ((Joc) context).mostrarPantallaFin(true); // Gana
                    }
                } else {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            carta1.girar();
                            carta2.girar();
                            notifyDataSetChanged();
                            torn = 0;
                        }
                    }, 1000);
                }
            }
        }
    }

    public CartaAdapter(List<Carta> cartes, Context context){
        this.cartes = cartes;
        this.context = context;
    }


    @Override
    public CartaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_carta, parent, false);
        return new CartaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CartaViewHolder holder, int position) {
        holder.imatge.setImageResource(cartes.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return cartes.size();
    }

    private boolean haGanado() {
        for (Carta carta : cartes) {
            if (carta.getEstat() != Carta.Estat.FIXED) {
                return false;
            }
        }
        return true;
    }
}
