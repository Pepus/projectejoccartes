package com.pepus.jocmemoriacartes;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class FiJoc extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin_juego);

        ImageView imgResultado = findViewById(R.id.imgResultado);
        TextView txtMensaje = findViewById(R.id.txtMensaje);
        ImageButton btnReiniciar = findViewById(R.id.btnReiniciar);
        ImageButton btnSalir = findViewById(R.id.btnSalir);

        boolean hasGanado = getIntent().getBooleanExtra("resultado", false);

        if (hasGanado) {
            imgResultado.setImageResource(R.drawable.final_winner); // Imagen de victoria
            txtMensaje.setText("Felicitas amore, ets una crack! Has guanyat!");
        } else {
            imgResultado.setImageResource(R.drawable.final_looser); // Imagen de derrota
            txtMensaje.setText("Oh, quina pena, guapi, has perdut. Pero no pateixis, pots tornar a començar!");
        }

        btnReiniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FiJoc.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }
}
