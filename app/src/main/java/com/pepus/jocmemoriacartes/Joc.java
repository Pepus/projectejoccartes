package com.pepus.jocmemoriacartes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Joc extends AppCompatActivity {

    private Cronometre cronometre;
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    private TextView tvTemporitzador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joc);
        // Obtener el nivel desde el intent
        int nivell = getIntent().getExtras().getInt("nivell");
        // Iniciar el temporizador
        tvTemporitzador = findViewById(R.id.tvTemporitzador);
        if (cronometre != null) {
            cronometre.cancel(); // Detiene el temporizador anterior si existe
        }
        cronometre = new Cronometre(nivell * 10000, 1000, tvTemporitzador, this);
        cronometre.start();
        List<Carta> cartes = new ArrayList<>();
        List<Integer> cartesDisponibles = new ArrayList<>();

        // Llenar la lista con los identificadores de las 19 cartas
        for (int i = 0; i <= 29; i++) {
            int idCarta = getResources().getIdentifier("c" + i, "drawable", getPackageName());
            cartesDisponibles.add(idCarta);
        }

        // Barajar la lista de cartas disponibles
        Collections.shuffle(cartesDisponibles);

        // Seleccionar las primeras 'nivell' cartas de la lista barajada
        List<Integer> cartesSeleccionades = cartesDisponibles.subList(0, nivell);

        // Crear los pares de cartas seleccionadas
        for (int idCarta : cartesSeleccionades) {
            cartes.add(new Carta(idCarta, R.drawable.back));
            cartes.add(new Carta(idCarta, R.drawable.back));
        }

        // Mezclar las cartas antes de mostrarlas
        Collections.shuffle(cartes);

        // Configurar el RecyclerView
        recycler = findViewById(R.id.recyclerView);
        recycler.setHasFixedSize(true);
        lManager = new GridLayoutManager(this, 4);
        recycler.setLayoutManager(lManager);
        adapter = new CartaAdapter(cartes, this);
        recycler.setAdapter(adapter);

    }

    public void mostrarPantallaFin(boolean haGanado) {
        Intent intent = new Intent(Joc.this, FiJoc.class);
        intent.putExtra("resultado", haGanado); // Cambia "ganado" o "perdido" a un booleano
        startActivity(intent);
        finish();  // Finaliza la partida actual
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cronometre != null) {
            cronometre.cancel(); // Detiene el temporizador
        }
    }

}
