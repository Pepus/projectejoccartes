package com.pepus.jocmemoriacartes;

import android.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;

import android.os.CountDownTimer;
import android.widget.TextView;

public class Cronometre extends CountDownTimer {

    private TextView txtCrono;
    Context context;


    public Cronometre(long millisInFuture, long countDownInterval, TextView textView, Joc joc) {
        super(millisInFuture, countDownInterval);
        this.txtCrono = textView;
        this.context = joc;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        txtCrono.setText ("Corre amore! Només falten: " + millisUntilFinished / 1000 + " segons");
    }

    @Override
    public void onFinish() {
        txtCrono.setText ("S'ha acabat el temps!!!");
        if (context instanceof Joc) {
            ((Joc) context).mostrarPantallaFin(false); // Pierde
        }
    }

}
