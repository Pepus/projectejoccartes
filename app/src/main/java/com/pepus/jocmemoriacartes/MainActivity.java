package com.pepus.jocmemoriacartes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton botoFacil;
    ImageButton botoMig;
    ImageButton botoDificil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botoFacil = findViewById(R.id.btnFacil);
        botoMig = findViewById(R.id.btnMig);
        botoDificil = findViewById(R.id.btnDificil);

        botoFacil.setOnClickListener(this);
        botoMig.setOnClickListener(this);
        botoDificil.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(MainActivity.this, Joc.class);

        switch (v.getId()) {

            case R.id.btnFacil:
                intent.putExtra("nivell", 4);
                startActivity(intent);
                break;

            case R.id.btnMig:
                intent.putExtra("nivell", 6);
                startActivity(intent);
                break;

            case R.id.btnDificil:
                intent.putExtra("nivell", 8);
                startActivity(intent);
                break;

            default:
                break;
        }
    }
}
