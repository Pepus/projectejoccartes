package com.pepus.jocmemoriacartes;

public class Carta {

    public enum Estat {FIXED, BACK, FRONT}

    private int frontimage;
    private int backimage;
    private Estat estat;

    public Carta(int frontimage, int backimage) {
        this.frontimage = frontimage;
        this.backimage = backimage;
        this.estat = Estat.BACK;
    }

    public int getFrontimage() {
        return frontimage;
    }

    public void setFrontimage(int frontimage) {
        this.frontimage = frontimage;
    }

    public int getBackimage() {
        return backimage;
    }

    public void setBackimage(int backimage) {
        this.backimage = backimage;
    }

    public Estat getEstat() {
        return estat;
    }

    public void setEstat(Estat estat) {
        this.estat = estat;
    }

    public int getImage() {
        int num = 0;
        switch (estat) {
            case FIXED:
                num = frontimage;
                break;
            case BACK:
                num = backimage;
                break;
            case FRONT:
                num = frontimage;
                break;
        }
        return num;
    }

    public void girar() {
        if(estat == Estat.BACK){
            this.setEstat(Estat.FRONT);
        }else if(estat == Estat.FRONT){
            this.setEstat(Estat.BACK);
        }
    }
}
